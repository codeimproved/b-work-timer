/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#ifndef TIMETRACKER_H
#define TIMETRACKER_H

#include <QObject>
#include <QTime>
#include <QTimer>
#include <QScopedPointer>
#include "BwtNotification.h"
#include "IdleMonitor.h"

struct BreakOptions
{
    static const int DefaultRestBreakIntervalMinutes = 45;
    static const int DefaultEyeBreakIntervalMinutes = 20;
    static const int DefaultPostponeMinutes = 5;
    static const int DefaultBreakTimeSeconds = 240;
    static const int DefaultEyeTimeSeconds = 15;
    static const int NaturalRestBreakSeconds = DefaultBreakTimeSeconds;

    BreakOptions()
        : restBreakIntervalMinutes(DefaultRestBreakIntervalMinutes)
        , eyeBreakIntervalMinutes(DefaultEyeBreakIntervalMinutes)
        , postponeMinutes(DefaultPostponeMinutes)
        , restBreakSeconds(DefaultBreakTimeSeconds)
        , eyeBreakSeconds(DefaultEyeTimeSeconds)
    {

    }

    int restBreakIntervalMinutes;
    int eyeBreakIntervalMinutes;
    int postponeMinutes;
    int restBreakSeconds;
    int eyeBreakSeconds;
};

// Tracks time and informs other components when it's time for a break.
class TimeTracker : public QObject
{
    Q_OBJECT
public:
    explicit TimeTracker(const BreakOptions& options = BreakOptions());

    int getRestBreakMinutes() const;
    void setRestBreakMinutes(int minutes);

    int getEyeBreakMinutes() const;
    void setEyeBreakMinutes(int minutes);

    int getPostponeMinutes() const;
    void setPostponeMinutes(int minutes);

    int getRestBreakDurationSeconds() const;
    void setRestBreakDurationSeconds(int seconds);

    int getEyeBreakDurationSeconds() const;
    void setEyeBreakDurationSeconds(int seconds);

    void postponeRestBreak();
    void postponeEyeBreak();

    void finishRestBreak();
    void finishEyeBreak();

    Q_SIGNAL void shouldShowRestNotification(int seconds);
    Q_SIGNAL void shouldShowEyeNotification(int seconds);
    Q_SIGNAL void updateBreakInfoText(const QString& text);

private:
    Q_SLOT void onTimerTick();
    Q_SLOT void onRestBreakActivated();
    Q_SLOT void onEyeBreakActivated();
    void sendBreakInfoText();

    int mRestBreakIntervalMinutes;
    int mEyeBreakIntervalMinutes;
    int mPostponeMinutes;
    int mRestBreakDurationSeconds;
    int mEyeBreakDurationSeconds;

    bool mRestIsActive;
    bool mEyeIsActive;

    BwtNotification mRestNotification;
    BwtNotification mEyeNotification;
    QScopedPointer<IdleMonitor> mIdleMonitor;

    QTimer mTimer;
};

#endif // TIMETRACKER_H

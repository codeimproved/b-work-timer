# README #

BWT is a small app that reminds you to take breaks from working on the computer. I originally created this as a quick and dirty app at the beginning of 2014 and decided to open source it now. It should work on Windows and Mac OS X.

As one can see, it's really spartan (doesn't even have an icon), but I do plan to make small improvements when time permits. Check out the issues page to see what's planned.

### How do I get set up? ###

* Right now one has to check it out and compile it using Qt. There are no other 3rd party dependencies.
* Binary downloads for Mac OS X and Windows will be provided when they're done and ready :-)

### Who do I talk to? ###

* Me - support@codeimproved.net
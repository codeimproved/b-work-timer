/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "BwtNotification.h"
#include "QsLog.h"
#include <cassert>

BwtNotification::BwtNotification(QObject* parent)
    : QObject(parent)
    , mPostponedSeconds(0)
    , mIntervalInSeconds(45 * 60)
    , mElapsedSeconds(0)
    , mLastActivation(QTime::currentTime())
{

}

void BwtNotification::postpone(int seconds)
{
    assert(seconds > 0);
    mPostponedSeconds += seconds;
}

void BwtNotification::addTimeElapsed(int seconds)
{
    mElapsedSeconds += seconds;
    if (mElapsedSeconds >= mIntervalInSeconds + mPostponedSeconds)
            emit activate();

    QLOG_TRACE() << "[bwt] notification check" << mElapsedSeconds << "out of"
                 << mIntervalInSeconds << "with" << mPostponedSeconds << "postponed";
}

void BwtNotification::setActivationIntervalInSeconds(int seconds)
{
    assert(seconds > 0);
    mIntervalInSeconds = seconds;
}

void BwtNotification::resetLastActivation()
{
    mPostponedSeconds = 0;
    mElapsedSeconds = 0;
    mLastActivation = QTime::currentTime();
}

QString BwtNotification::getRemainingTimeAsText() const
{
    const int remainingSeconds = std::max(0, mIntervalInSeconds + mPostponedSeconds - mElapsedSeconds);
    if (remainingSeconds < 60) {
        return QString("< 1m");
    }
    return QString("%1m").arg(remainingSeconds / 60);
}

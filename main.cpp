/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "MainWindow.h"
#include "QsLog.h"
#include "QsLogDest.h"
#include <QDir>
#include <QApplication>
#include <QStandardPaths>
#include <algorithm>

void setUpLogging()
{
    QsLogging::Logger& logger = QsLogging::Logger::instance();
    logger.setLoggingLevel(QsLogging::TraceLevel);
    const QString cacheFolderPath = *QStandardPaths::standardLocations(QStandardPaths::CacheLocation).begin();
    QDir cacheDir(cacheFolderPath);
    cacheDir.mkpath(cacheFolderPath);
    const QString sLogPath(cacheDir.filePath("log.txt"));

    QsLogging::DestinationPtr fileDestination =
        QsLogging::DestinationFactory::MakeFileDestination(sLogPath, QsLogging::EnableLogRotation,
                                                           QsLogging::MaxSizeBytes(100000),
                                                           QsLogging::MaxOldLogCount(2));
    QsLogging::DestinationPtr debugDestination(QsLogging::DestinationFactory::MakeDebugOutputDestination());
    logger.addDestination(debugDestination);
    logger.addDestination(fileDestination);

    QLOG_INFO() << "Program started, log file available at" << cacheFolderPath;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("BWorkTimer");
    a.setApplicationDisplayName("BWT");
    a.setOrganizationName("CodeImproved");
    a.setOrganizationDomain("codeimproved.net");
    a.setQuitOnLastWindowClosed(false);
    a.addLibraryPath(a.applicationDirPath());

    setUpLogging();

    QLOG_INFO() << "Built with Qt" << QT_VERSION_STR << "running on" << qVersion();

    const QStringList minArguments = {"--minimized", "--min", "--minimised", "-m"};
    const QStringList appArguments = a.arguments();
    auto argumentIt = std::find_first_of(appArguments.begin(), appArguments.end(),
                                      minArguments.begin(), minArguments.end());

    MainWindow w;
    if (argumentIt == appArguments.end()) {
        w.showNormal();
    } else {
        QLOG_INFO() << "...started minimized";
    }

    return a.exec();
}

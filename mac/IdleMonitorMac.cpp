/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "IdleMonitorMac.h"
#include <IOKit/IOKitLib.h>
#include <CoreFoundation/CoreFoundation.h>
#include <cmath>

IdleMonitorMac::IdleMonitorMac()
{
}

// Source: adapted from stackoverflow.com
int64_t IdleMonitorMac::getIdleSeconds() {
    int64_t idleSeconds = -1;
    io_iterator_t iter = 0;
    CFNumberRef objIdleTime = 0;
    io_registry_entry_t entry = 0;
    CFMutableDictionaryRef dict = 0;
    if (IOServiceGetMatchingServices(kIOMasterPortDefault, IOServiceMatching("IOHIDSystem"), &iter) != KERN_SUCCESS)
        goto cleanup;

    entry = IOIteratorNext(iter);
    if (!entry)
        goto cleanup;

    dict = NULL;
    if (IORegistryEntryCreateCFProperties(entry, &dict, kCFAllocatorDefault, 0) != KERN_SUCCESS)
        goto cleanup;

    objIdleTime = (CFNumberRef)CFDictionaryGetValue(dict, CFSTR("HIDIdleTime"));
    if (objIdleTime) {
        int64_t nanoseconds = 0;
        if (CFNumberGetValue(objIdleTime, kCFNumberSInt64Type, &nanoseconds))
            idleSeconds = nanoseconds / pow(10,9);
    }

cleanup:
    if (dict)
        CFRelease(dict);
    if (entry)
        IOObjectRelease(entry);
    if (iter)
        IOObjectRelease(iter);

    return idleSeconds;
}

/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#ifndef IDLESECONDSMAC_H
#define IDLESECONDSMAC_H

#include "../IdleMonitor.h"

class IdleMonitorMac : public IdleMonitor
{
public:
    IdleMonitorMac();

    virtual int64_t getIdleSeconds();
};



#endif // IDLESECONDSMAC_H

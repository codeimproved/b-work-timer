/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "TimeTracker.h"
#include "NotificationWindow.h"
#include <QMainWindow>
#include <QSoundEffect>
#include <QSystemTrayIcon>
class QString;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void onActivatedFromTray(QSystemTrayIcon::ActivationReason reason);

private slots:
    void onRestBreakChanged(int value);
    void onEyeBreakChanged(int value);
    void onPostponeChanged(int value);
    void onRestDurationChanged(int value);
    void onEyeDurationChanged(int value);
    void onShowRestBreak(int seconds);
    void onShowEyeBreak(int seconds);
    void onUpdateBreakInfoText(const QString& text);
    void onPostponedRestBreak();
    void onPostponedEyeBreak();
    void onFinishRestBreak();
    void onFinishEyeBreak();
    void saveSettings();

private:
    Ui::MainWindow *ui;
    NotificationWindow* mRestNotificationWindow;
    NotificationWindow* mEyeNotificationWindow;
    QSystemTrayIcon* mTrayIcon;
    TimeTracker mTracker;
    QSoundEffect mRestSound;
    QSoundEffect mEyeSound;
};

#endif // MAINWINDOW_H

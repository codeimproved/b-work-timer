QT += core gui widgets multimedia

TARGET = BWT
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
        MainWindow.cpp \
    IdleMonitor.cpp \
    TimeTracker.cpp \
    NotificationWindow.cpp \
    BwtNotification.cpp

HEADERS  += MainWindow.h \
    IdleMonitor.h \
    TimeTracker.h \
    NotificationWindow.h \
    BwtNotification.h

FORMS    += MainWindow.ui \
    NotificationWindow.ui

macx {
    SOURCES += mac/IdleMonitorMac.cpp
    HEADERS += mac/IdleMonitorMac.h

    QMAKE_INFO_PLIST = mac/Info.plist

    LIBS += -framework IoKit \
        -framework CoreFoundation
}

win32 {
    SOURCES += windows/IdleMonitorWindows.cpp
    HEADERS += windows/IdleMonitorWindows.h
}

include(QsLog/QsLog.pri)

RESOURCES += \
    resources.qrc

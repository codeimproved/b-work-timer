/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "NotificationWindow.h"
#include "ui_NotificationWindow.h"

static const char *sRestMesages[] = {
    QT_TRANSLATE_NOOP("RestBreak", "Take a rest break. Get up from your seat and walk around for"
                      " a few minutes."),
    QT_TRANSLATE_NOOP("EyeBreak", "Take a moment to rest your eyes."
                      " Focus your vision in the distance, blink a few times, close your eyes for a few seconds and open them again.")
};

enum NotificationType
{
    NotificationRest    = 0,
    NotificationEyeRest = 1
};

NotificationWindow::NotificationWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NotificationWindow)
{
    ui->setupUi(this);

    mTimer.setInterval(1000);
    mTimer.setTimerType(Qt::VeryCoarseTimer);

    setWindowFlags(Qt::WindowStaysOnTopHint | Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

    connect(ui->btnPostpone, SIGNAL(clicked()), SLOT(onPostponeClicked()));
    connect(&mTimer, SIGNAL(timeout()), SLOT(onTimerTick()));
}

NotificationWindow::~NotificationWindow()
{
    delete ui;
}

void NotificationWindow::showRestBreak(int seconds)
{
    ui->txtBreakMessage->setPlainText(sRestMesages[NotificationRest]);
    ui->progressBar->setMaximum(seconds);
    ui->progressBar->setValue(0);
    ui->lblRemaining->setText(QString("Time to go: %1 minutes, %2 seconds")
                              .arg(mSecondsInBreak / 60)
                              .arg(mSecondsInBreak % 60));
    mSecondsInBreak = seconds;
    updateRemainingTimeLabel();
    mTimer.start();
    show();
}


void NotificationWindow::showEyeBreak(int seconds)
{
    ui->txtBreakMessage->setPlainText(sRestMesages[NotificationEyeRest]);
    ui->progressBar->setMaximum(seconds);
    ui->progressBar->setValue(0);
    mSecondsInBreak = seconds;
    updateRemainingTimeLabel();
    mTimer.start();
    show();
}

void NotificationWindow::onTimerTick()
{
    --mSecondsInBreak;
    if (mSecondsInBreak <= 0) {
        mTimer.stop();
        emit breakCompleted();
        close();
    }

    ui->progressBar->setValue(ui->progressBar->maximum() - mSecondsInBreak);
    updateRemainingTimeLabel();
}

void NotificationWindow::onPostponeClicked()
{
    mTimer.stop();
    emit postponed();

    close();
}

void NotificationWindow::updateRemainingTimeLabel()
{
    ui->lblRemaining->setText(QString("Time to go: %1 minutes, %2 seconds")
                              .arg(mSecondsInBreak / 60)
                              .arg(mSecondsInBreak % 60));
}


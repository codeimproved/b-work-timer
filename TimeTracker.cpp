/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "TimeTracker.h"
#ifdef Q_OS_OSX
#include "mac/IdleMonitorMac.h"
#elif defined(Q_OS_WIN)
#include "windows/IdleMonitorWindows.h"
#endif
#include "QsLog.h"
#include <algorithm>

static const int sTimerIntervalMillisec = 30000;

TimeTracker::TimeTracker(const BreakOptions &options)
    : mRestBreakIntervalMinutes(options.restBreakIntervalMinutes)
    , mEyeBreakIntervalMinutes(options.eyeBreakIntervalMinutes)
    , mPostponeMinutes(options.postponeMinutes)
    , mRestBreakDurationSeconds(options.restBreakSeconds)
    , mEyeBreakDurationSeconds(options.eyeBreakSeconds)
    , mRestIsActive(false)
    , mEyeIsActive(false)
    , mRestNotification(nullptr)
    , mEyeNotification(nullptr)
{
    mTimer.setInterval(sTimerIntervalMillisec);
    mTimer.setTimerType(Qt::CoarseTimer);

    connect(&mTimer, SIGNAL(timeout()), SLOT(onTimerTick()));
    connect(&mRestNotification, SIGNAL(activate()), SLOT(onRestBreakActivated()));
    connect(&mEyeNotification, SIGNAL(activate()), SLOT(onEyeBreakActivated()));

#ifdef Q_OS_OSX
    mIdleMonitor.reset(new IdleMonitorMac);
#elif defined(Q_OS_WIN)
    mIdleMonitor.reset(new IdleMonitorWindows);
#endif

    mTimer.start();
}


int TimeTracker::getRestBreakMinutes() const
{
    return mRestBreakIntervalMinutes;
}

void TimeTracker::setRestBreakMinutes(int minutes)
{
    QLOG_DEBUG() << "[tracker] rest break minutes is now" << minutes;
    mRestBreakIntervalMinutes = minutes;
    mRestNotification.setActivationIntervalInSeconds(minutes * 60);
    sendBreakInfoText();
}
int TimeTracker::getEyeBreakMinutes() const
{
    return mEyeBreakIntervalMinutes;
}

void TimeTracker::setEyeBreakMinutes(int minutes)
{
    QLOG_DEBUG() << "[tracker] eye break minutes is now" << minutes;
    mEyeBreakIntervalMinutes = minutes;
    mEyeNotification.setActivationIntervalInSeconds(minutes * 60);
    sendBreakInfoText();
}
int TimeTracker::getPostponeMinutes() const
{
    return mPostponeMinutes;
}

void TimeTracker::setPostponeMinutes(int minutes)
{
    QLOG_DEBUG() << "[tracker] postpone minutes is now" << minutes;
    mPostponeMinutes = minutes;
}

int TimeTracker::getEyeBreakDurationSeconds() const
{
    return mEyeBreakDurationSeconds;
}

void TimeTracker::setEyeBreakDurationSeconds(int seconds)
{
    QLOG_DEBUG() << "[tracker] eye break duration is now" << seconds << "seconds";
    mEyeBreakDurationSeconds = seconds;
}

int TimeTracker::getRestBreakDurationSeconds() const
{
    return mRestBreakDurationSeconds;
}

void TimeTracker::setRestBreakDurationSeconds(int seconds)
{
    QLOG_DEBUG() << "[tracker] rest break duration is now" << seconds << "seconds";
    mRestBreakDurationSeconds = seconds;
}

void TimeTracker::postponeRestBreak()
{
    QLOG_DEBUG() << "[tracker] postponing rest break...";
    mRestIsActive = false;
    mRestNotification.postpone(mPostponeMinutes * 60);
    sendBreakInfoText();
}

void TimeTracker::postponeEyeBreak()
{
    QLOG_DEBUG() << "[tracker] postponing eye break...";
    mEyeIsActive = false;
    mEyeNotification.postpone(mPostponeMinutes * 60);
    sendBreakInfoText();
}

void TimeTracker::finishRestBreak()
{
    QLOG_DEBUG() << "[tracker] finished rest break!";
    mRestNotification.resetLastActivation();
    mEyeNotification.resetLastActivation();
    mRestIsActive = false;
    sendBreakInfoText();
}

void TimeTracker::finishEyeBreak()
{
    QLOG_DEBUG() << "[tracker] finished eye break!";    
    mEyeNotification.resetLastActivation();
    mEyeIsActive = false;
    sendBreakInfoText();
}

void TimeTracker::onTimerTick()
{
    const int idleSeconds = static_cast<int>(mIdleMonitor->getIdleSeconds());
    if (-1 == idleSeconds) {
        QLOG_ERROR() << "[tracker] idle monitor failed to get time";
        return;
    }

    // Only mark time as elapsed if there's user activity happening, don't add time
    // when already showing a notification. Rest breaks have priority over eye breaks.
    const int timerIntervalInSeconds = mTimer.interval() / 1000;
    const int elapsedTime = std::max(0, timerIntervalInSeconds - idleSeconds);
    if (!mRestIsActive) {
        if (idleSeconds >= BreakOptions::NaturalRestBreakSeconds) {
            QLOG_DEBUG() << "[tracker] natural rest break detected";
            mRestNotification.resetLastActivation();
        }
        else
            mRestNotification.addTimeElapsed(elapsedTime);
    }
    else {
        QLOG_TRACE() << "[tracker] skipping rest break time, notification is active";
    }
    if (!mEyeIsActive && !mRestIsActive) {
        if (idleSeconds >= BreakOptions::NaturalRestBreakSeconds) {
            QLOG_DEBUG() << "[tracker] natural eye break detected";
            mEyeNotification.resetLastActivation();
        }
        else {
            mEyeNotification.addTimeElapsed(elapsedTime);
        }
    }
    else {
        QLOG_TRACE() << "[tracker] skipping eye break time, notification is active";
    }

    QLOG_TRACE() << "[tracker] added" << elapsedTime << "seconds";

    sendBreakInfoText();
}

void TimeTracker::onRestBreakActivated()
{
    mRestIsActive = true;
    emit shouldShowRestNotification(mRestBreakDurationSeconds);
}

void TimeTracker::onEyeBreakActivated()
{
    mEyeIsActive = true;
    emit shouldShowEyeNotification(mEyeBreakDurationSeconds);
}

void TimeTracker::sendBreakInfoText()
{
    const QString updateText = QString("Rest break: %1. Eye break: %2").arg(mRestNotification.getRemainingTimeAsText()).arg(mEyeNotification.getRemainingTimeAsText());
    emit updateBreakInfoText(updateText);
}

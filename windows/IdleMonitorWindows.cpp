/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "IdleMonitorWindows.h"
#define WINDOWS_LEAN_AND_MEAN
#include <Windows.h>
#include "QsLog.h"

int64_t IdleMonitorWindows::getIdleSeconds()
{
    LASTINPUTINFO inputInfo;
    inputInfo.cbSize = sizeof(LASTINPUTINFO);
    if (0 == ::GetLastInputInfo(&inputInfo))
        return -1;

    const DWORD tickCount = ::GetTickCount();
    const int elapsed = (tickCount - inputInfo.dwTime) / 1000;

    QLOG_TRACE() << "[idle] ticks" << tickCount << "last input" << inputInfo.dwTime << "elapsed" << elapsed;
    return elapsed;
}

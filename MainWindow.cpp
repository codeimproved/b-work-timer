/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#include "MainWindow.h"
#include "ui_MainWindow.h"
#include "QsLog.h"
#include "NotificationWindow.h"
#include <QSettings>
#include <QSoundEffect>
#include <QMenu>

static const QLatin1Literal sRestBreakKey("RestBreakMinutes");
static const QLatin1Literal sEyeBreakKey("EyeBreakMinutes");
static const QLatin1Literal sPostponeKey("PostponeMinutes");
static const QLatin1Literal sRestBreakDurationKey("RestBreakDuration");
static const QLatin1Literal sEyeBreakDurationKey("EyeBreakDuration");

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , mRestNotificationWindow(nullptr)
    , mEyeNotificationWindow(nullptr)
    , mTrayIcon(nullptr)
{
    ui->setupUi(this);
    setWindowFlags(Qt::WindowStaysOnTopHint | windowFlags());

    connect(ui->btnHide, SIGNAL(clicked()), SLOT(hide()));

    QSettings s;
    mTracker.setRestBreakMinutes(s.value(sRestBreakKey, BreakOptions::DefaultRestBreakIntervalMinutes).toInt());
    mTracker.setEyeBreakMinutes(s.value(sEyeBreakKey, BreakOptions::DefaultEyeBreakIntervalMinutes).toInt());
    mTracker.setPostponeMinutes(s.value(sPostponeKey, BreakOptions::DefaultPostponeMinutes).toInt());
    mTracker.setRestBreakDurationSeconds(s.value(sRestBreakDurationKey, BreakOptions::DefaultBreakTimeSeconds).toInt());
    mTracker.setEyeBreakDurationSeconds(s.value(sEyeBreakDurationKey, BreakOptions::DefaultEyeTimeSeconds).toInt());
    ui->edtRestBreaks->setValue(mTracker.getRestBreakMinutes());
    ui->edtEyeBreaks->setValue(mTracker.getEyeBreakMinutes());
    ui->edtPostpone->setValue(mTracker.getPostponeMinutes());
    ui->edtRestDuration->setValue(mTracker.getRestBreakDurationSeconds() / 60);
    ui->edtEyeDuration->setValue(mTracker.getEyeBreakDurationSeconds());

    mRestSound.setSource(QUrl::fromLocalFile(":/notify.wav"));
    mRestSound.setLoopCount(1);
    mRestSound.setVolume(0.5f);
    mEyeSound.setSource(QUrl::fromLocalFile(":/notifyshort.wav"));
    mEyeSound.setLoopCount(1);
    mEyeSound.setVolume(0.30f);

    QMenu* menu = new QMenu(this);
#ifdef Q_OS_MACX
    menu->setDefaultAction(menu->addAction(qApp->applicationName()));
    menu->addSeparator();
#endif
    menu->addAction(QIcon(":/showIcon"), QObject::tr("Show &settings"), this, SLOT(showNormal()));
    menu->addAction(QIcon(":/exitIcon"), QObject::tr("E&xit"), qApp, SLOT(quit()));
    mTrayIcon = new QSystemTrayIcon(this);
    mTrayIcon->setIcon(QIcon(":/mainIcon"));
    mTrayIcon->setContextMenu(menu);
    connect(mTrayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onActivatedFromTray(QSystemTrayIcon::ActivationReason)));
    mTrayIcon->show();

    mRestNotificationWindow = new NotificationWindow(this);
    mEyeNotificationWindow  = new NotificationWindow(this);
    connect(mRestNotificationWindow, SIGNAL(postponed()), SLOT(onPostponedRestBreak()));
    connect(mEyeNotificationWindow, SIGNAL(postponed()), SLOT(onPostponedEyeBreak()));
    connect(mRestNotificationWindow, SIGNAL(breakCompleted()), SLOT(onFinishRestBreak()));
    connect(mEyeNotificationWindow, SIGNAL(breakCompleted()), SLOT(onFinishEyeBreak()));

    connect(ui->edtRestBreaks, SIGNAL(valueChanged(int)), SLOT(onRestBreakChanged(int)));
    connect(ui->edtEyeBreaks, SIGNAL(valueChanged(int)), SLOT(onEyeBreakChanged(int)));
    connect(ui->edtRestDuration, SIGNAL(valueChanged(int)), SLOT(onRestDurationChanged(int)));
    connect(ui->edtEyeDuration, SIGNAL(valueChanged(int)), SLOT(onEyeDurationChanged(int)));
    connect(ui->edtPostpone, SIGNAL(valueChanged(int)), SLOT(onPostponeChanged(int)));

    connect(&mTracker, SIGNAL(shouldShowRestNotification(int)), SLOT(onShowRestBreak(int)));
    connect(&mTracker, SIGNAL(shouldShowEyeNotification(int)), SLOT(onShowEyeBreak(int)));
    connect(&mTracker, SIGNAL(updateBreakInfoText(QString)), this, SLOT(onUpdateBreakInfoText(QString)));

    connect(qApp, SIGNAL(aboutToQuit()), SLOT(saveSettings()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onActivatedFromTray(QSystemTrayIcon::ActivationReason reason)
{
    if (reason == QSystemTrayIcon::DoubleClick) {
        showNormal();
        activateWindow();
    }
}

void MainWindow::onRestBreakChanged(int value)
{
    mTracker.setRestBreakMinutes(value);
}

void MainWindow::onEyeBreakChanged(int value)
{
    mTracker.setEyeBreakMinutes(value);
}

void MainWindow::onPostponeChanged(int value)
{
    mTracker.setPostponeMinutes(value);
}

void MainWindow::onRestDurationChanged(int value)
{
    mTracker.setRestBreakDurationSeconds(value * 60);
}

void MainWindow::onEyeDurationChanged(int value)
{
    mTracker.setEyeBreakDurationSeconds(value);
}

void MainWindow::onShowRestBreak(int seconds)
{
    QLOG_DEBUG() << "Rest break time!";
    if (mEyeNotificationWindow->isVisible()) {
        QLOG_DEBUG() << "...superseding eye break";
        mEyeNotificationWindow->close();
    }
    mRestNotificationWindow->showRestBreak(seconds);
}

void MainWindow::onShowEyeBreak(int seconds)
{
    QLOG_DEBUG() << "Eye break time!";
    if (mRestNotificationWindow->isVisible()) {
        QLOG_DEBUG() << "...nevermind, already in a rest break";
        return;
    }

    mEyeNotificationWindow->showEyeBreak(seconds);
}

void MainWindow::onUpdateBreakInfoText(const QString &text)
{
#ifdef Q_OS_MACX
    mTrayIcon->contextMenu()->defaultAction()->setText(text);
#else
    mTrayIcon->setToolTip(text);
#endif
}

void MainWindow::onPostponedRestBreak()
{
    mTracker.postponeRestBreak();
}

void MainWindow::onPostponedEyeBreak()
{
    mTracker.postponeEyeBreak();
}

void MainWindow::onFinishRestBreak()
{
    mTracker.finishRestBreak();
    mRestSound.play();
}

void MainWindow::onFinishEyeBreak()
{
    mTracker.finishEyeBreak();
    mEyeSound.play();
}

void MainWindow::saveSettings()
{
    QSettings s;
    s.setValue(sRestBreakKey, mTracker.getRestBreakMinutes());
    s.setValue(sEyeBreakKey, mTracker.getEyeBreakMinutes());
    s.setValue(sPostponeKey, mTracker.getPostponeMinutes());
    s.setValue(sRestBreakDurationKey, mTracker.getRestBreakDurationSeconds());
    s.setValue(sEyeBreakDurationKey, mTracker.getEyeBreakDurationSeconds());
    s.sync();
}


/****************************************************************************
**
** Copyright 2014-2015 (C) razvanpetru
** All rights reserved.
** Contact: support@codeimproved.net
**
** This file is part of the BWT app
**
** GNU General Public License Usage
** This file may be used under the terms of the GNU General
** Public License version 3.0 as published by the Free Software Foundation
** and appearing in the file LICENSE.GPL included in the packaging of this
** file. Please review the following information to ensure the GNU General
** Public License version 3.0 requirements will be met:
** http://www.gnu.org/copyleft/gpl.html.
**
** Other Usage
** Alternatively, this file may be used in accordance with the terms and
** conditions contained in a signed written agreement between you and the author.
**
****************************************************************************/
#ifndef NOTIFICATIONWINDOW_H
#define NOTIFICATIONWINDOW_H

#include <QWidget>
#include <QTimer>

namespace Ui {
class NotificationWindow;
}

class NotificationWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NotificationWindow(QWidget *parent = 0);
    ~NotificationWindow();

    void showRestBreak(int seconds);
    void showEyeBreak(int seconds);

    Q_SIGNAL void postponed();
    Q_SIGNAL void breakCompleted();
private:
    Q_SLOT void onTimerTick();
    Q_SLOT void onPostponeClicked();

    void updateRemainingTimeLabel();

    Ui::NotificationWindow *ui;
    QTimer mTimer;
    int mSecondsInBreak;
};

#endif // NOTIFICATIONWINDOW_H
